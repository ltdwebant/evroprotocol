//
//  DriveInfoPageViewControler.swift
//  CrashesARKit
//
//  Created by Danya on 11.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

protocol DriveInfoTextfieldDelegate: class {
	func textFieldDidBeginEditing()
	func textFieldReturn(with driversInfo: DriversInformation)
	func pressedNextButton(with state: PageInfoState, complition: @escaping (() -> Void))
	func pressedPreviousButton()
}

class DriveInfoPageViewController: UIViewController {
	
	open var driverState: PageInfoState = .myInfo
	open var driverInfo: DriversInformation = DriversInformation()
	
	weak var driverDelegate: DriveInfoTextfieldDelegate!
	
	@IBOutlet weak var carMark: UnderlineTextfield! {
		didSet {
			carMark.underlineDelegate = self
			carMark.tag = 0
		}
	}
	@IBOutlet weak var carModel: UnderlineTextfield! {
		didSet {
			carModel.underlineDelegate = self
			carModel.tag = 1
		}
	}
	@IBOutlet weak var carNumber: UnderlineTextfield! {
		didSet {
			carNumber.underlineDelegate = self
			carNumber.tag = 2
		}
	}
	
	@IBOutlet weak var ownerName: UnderlineTextfield! {
		didSet {
			ownerName.underlineDelegate = self
			ownerName.tag = 3
		}
	}
	@IBOutlet weak var ownerSurname: UnderlineTextfield! {
		didSet {
			ownerSurname.underlineDelegate = self
			ownerSurname.tag = 4
		}
	}
	@IBOutlet weak var ownerPatronymic: UnderlineTextfield! {
		didSet {
			ownerPatronymic.underlineDelegate = self
			ownerPatronymic.tag = 5
		}
	}
	
	@IBOutlet weak var yourCarLabel: UILabel!
	@IBOutlet weak var yourInfoLabel: UILabel!
	
	@IBOutlet weak var nextButton: NextButton! {
		
		didSet {
			nextButton.handler(for: .touchUpInside) {
				self.driverDelegate.pressedNextButton(with: self.driverState, complition: {
					if self.driverState == .anotherDriverInfo {
						self.checkAboutEmptyTextFields()
					}
				})
			}
		}
		
	}
	
	@IBOutlet weak var previousButton: NextButton! {
		didSet {
			switch self.driverState {
			case .myInfo:
				previousButton.alpha = 0
			case .anotherDriverInfo:
				previousButton.alpha = 1
				previousButton.handler(for: .touchUpInside, {
					self.driverDelegate.pressedPreviousButton()
				})
			}
		}
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		self.view.endEditing(true)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.previousButton.setTitle("Назад", for: .normal)
		self.resetInfo()
	}
	
	private func checkAboutEmptyTextFields() {
		for textfield in self.view.subviews where textfield is UnderlineTextfield {
			if (textfield as! UnderlineTextfield).text == "" {
				(textfield as! UnderlineTextfield).changeFieldColor(for: .empty)
			}
		}
	}
	
	private func resetInfo() {
		switch self.driverState {
		case .anotherDriverInfo:
			if self.driverInfo.anotherCarInfo?.mark != nil {
				self.carMark.text = self.driverInfo.anotherCarInfo?.mark
			}
			if self.driverInfo.anotherCarInfo?.model != nil {
				self.carModel.text = self.driverInfo.anotherCarInfo?.model
			}
			if self.driverInfo.anotherCarInfo?.registryNumber != nil {
				self.carNumber.text = self.driverInfo.anotherCarInfo?.registryNumber
			}
			if self.driverInfo.anotherDriverInfo?.name != nil {
				self.ownerName.text = self.driverInfo.anotherDriverInfo?.name
			}
			if self.driverInfo.anotherDriverInfo?.surname != nil {
				self.ownerSurname.text = self.driverInfo.anotherDriverInfo?.surname
			}
			if self.driverInfo.anotherDriverInfo?.patronymic != nil {
				self.ownerPatronymic.text = self.driverInfo.anotherDriverInfo?.patronymic
			}
		case .myInfo:
			if self.driverInfo.ownerCarInfo?.mark != nil {
				self.carMark.text = self.driverInfo.ownerCarInfo?.mark
			}
			if self.driverInfo.ownerCarInfo?.model != nil {
				self.carModel.text = self.driverInfo.ownerCarInfo?.model
			}
			if self.driverInfo.ownerCarInfo?.registryNumber != nil {
				self.carNumber.text = self.driverInfo.ownerCarInfo?.registryNumber
			}
			if self.driverInfo.ownerInfo?.name  != nil {
				self.ownerName.text = self.driverInfo.ownerInfo?.name
			}
			if self.driverInfo.ownerInfo?.surname != nil {
				self.ownerSurname.text = self.driverInfo.ownerInfo?.surname
			}
			if self.driverInfo.ownerInfo?.patronymic != nil {
				self.ownerPatronymic.text = self.driverInfo.ownerInfo?.patronymic
			}
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		switch driverState {
		case .myInfo:
			self.yourCarLabel.text = "Ваше транспортное средство"
			self.yourInfoLabel.text = "Ваши данные"
		case .anotherDriverInfo:
			self.yourCarLabel.text = "Второе транспортное средство"
			self.yourInfoLabel.text = "Данные другого водителя"
		}
	}
	
}

extension DriveInfoPageViewController: UnderlineTextfieldDelegate {
	func underlineTextfieldDidBeginEditing(_ textField: UITextField) {
		self.driverDelegate.textFieldDidBeginEditing()
	}
	
	func underlineTextfieldDidEndEditing(_ textField: UITextField) {
		let text = textField.text
		switch textField.tag {
		case 0:
			switch driverState {
			case .myInfo:
				self.driverInfo.ownerCarInfo!.mark = text
			case .anotherDriverInfo:
				self.driverInfo.anotherCarInfo!.mark = text
			}
		case 1:
			switch driverState {
			case .myInfo:
				self.driverInfo.ownerCarInfo!.model = text
			case .anotherDriverInfo:
				self.driverInfo.anotherCarInfo!.model = text
			}
		case 2:
			switch driverState {
			case .myInfo:
				self.driverInfo.ownerCarInfo!.registryNumber = text
			case .anotherDriverInfo:
				self.driverInfo.anotherCarInfo!.registryNumber = text
			}
		case 3:
			switch driverState {
			case .myInfo:
				self.driverInfo.ownerInfo!.name = text
			case .anotherDriverInfo:
				self.driverInfo.anotherDriverInfo!.name = text
			}
		case 4:
			switch driverState {
			case .myInfo:
				self.driverInfo.ownerInfo!.surname = text
			case .anotherDriverInfo:
				self.driverInfo.anotherDriverInfo!.surname = text
			}
		case 5:
			switch driverState {
			case .myInfo:
				self.driverInfo.ownerInfo!.patronymic = text
			case .anotherDriverInfo:
				self.driverInfo.anotherDriverInfo!.patronymic = text
			}
		default:
			break
		}
		self.driverDelegate.textFieldReturn(with: self.driverInfo)
	}
}
