//
//  DriveInfoViewController.swift
//  CrashesARKit
//
//  Created by Danya on 11.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

protocol DriveInfoWindowCloseDelegate: class {
	func schouldCloseWindow(with driversInfo: DriversInformation)
}

class DriveInfoViewController: UIViewController {

	weak var closeDelegate: DriveInfoWindowCloseDelegate!

	private var localAddress: String?
	private var localCrashDate: Date?
	
	@IBOutlet weak var dateCrashTextfield: UnderlineTextfield! {
		didSet {
			self.dateCrashTextfield.underlineDelegate = self
			let datePicker: UIDatePicker = UIDatePicker()
			datePicker.datePickerMode = .dateAndTime
			datePicker.handler(for: .valueChanged) {
				self.localCrashDate = datePicker.date
				let formatter = DateFormatter()
				formatter.dateFormat = "d-MMMM-yyyy HH:mm"
				self.dateCrashTextfield.text = formatter.string(from: self.localCrashDate!)
			}
			dateCrashTextfield.inputView = datePicker
		}
	}
	
	@IBOutlet weak var addressTextfield: UnderlineTextfield! {
		didSet {
			self.addressTextfield.underlineDelegate = self
			addressTextfield.tag = 1337
		}
	}
	
	@IBOutlet weak var containerView: UIView!
	
	fileprivate var driversInfo: DriversInformation!
	
	private(set) lazy var orderedViewControllers:[UIViewController] = {
		let startVC = self.viewAtIndex(driverState: PageInfoState.myInfo) as? DriveInfoPageViewController
		guard let start = startVC else {
			return [UIViewController()]
		}
		return [start]
	}()
	
	var pageController: UIPageViewController!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.driversInfo = DriversInformation()
		self.initializePageViewController()
		self.removeScrollGesturesFromPageView()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		self.view.endEditing(true)
	}
	
	private func removeScrollGesturesFromPageView() {
		for scrollView in self.pageController.view.subviews where  scrollView is UIScrollView {
			(scrollView as! UIScrollView).isScrollEnabled = false
		}
	}
	
	private func initializePageViewController() {
		self.pageController = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as? UIPageViewController
		self.pageController.delegate = self
		self.pageController.dataSource = self
		self.pageController.setViewControllers(orderedViewControllers, direction: .forward, animated: true, completion: nil)
		self.pageController.view.frame = CGRect(x: 0,
		                                        y: 0,
		                                        width: containerView.frame.width,
		                                        height: containerView.frame.height)
		self.containerView.addSubview(pageController.view)
		self.pageController.didMove(toParentViewController: self)
	}
}

extension DriveInfoViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
	func viewAtIndex(driverState: PageInfoState) -> UIViewController? {
		guard let VC: DriveInfoPageViewController = self.storyboard?.instantiateViewController(withIdentifier:
			String(describing: DriveInfoPageViewController.self)) as! DriveInfoPageViewController else {
				return nil
		}
		VC.driverState = driverState
		VC.driverInfo = self.driversInfo
		VC.driverDelegate = self
		return VC
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
		let VC = viewController as! DriveInfoPageViewController
		if VC.driverState == .anotherDriverInfo {
			return self.viewAtIndex(driverState: .myInfo)
		}
		return nil
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
		let VC = viewController as! DriveInfoPageViewController
		if VC.driverState == .myInfo {
			return self.viewAtIndex(driverState: .anotherDriverInfo)
		}
		return nil
	}
	
}

extension DriveInfoViewController: DriveInfoTextfieldDelegate {

	
	func pressedPreviousButton() {
		self.pageController.setViewControllers([(self.viewAtIndex(driverState: .myInfo) as? DriveInfoPageViewController)!],
		                                       direction: .reverse,
		                                       animated: true,
		                                       completion: nil)
	}
	
	func pressedNextButton(with state: PageInfoState, complition: @escaping (() -> Void)) {
		switch state {
		case .myInfo:
			self.pageController.setViewControllers([(self.viewAtIndex(driverState: .anotherDriverInfo) as? DriveInfoPageViewController)!],
																				 direction: .forward,
																				 animated: true,
																				 completion: nil)
		case .anotherDriverInfo:
			guard closeDelegate != nil else {
				return
			}
			self.driversInfo.crashStreet = self.localAddress
			self.driversInfo.crashDate = self.localCrashDate
			if CheckingClasses.checkEmpty(driverInformation: self.driversInfo) == true {
				self.closeDelegate.schouldCloseWindow(with: self.driversInfo)
			} else {
				if self.addressTextfield.text!.isEmpty {
					self.addressTextfield.changeFieldColor(for: .empty)
				}
				if self.dateCrashTextfield.text!.isEmpty  {
					self.dateCrashTextfield.changeFieldColor(for: .empty)
				}
				complition()
			}
		}
	}
	
	func textFieldDidBeginEditing() {
		UIView.animate(withDuration: 0.2) {
			self.view.window?.frame.origin.y -= 200
		}
	}
	
	func textFieldReturn(with driversInfo: DriversInformation) {
		UIView.animate(withDuration: 0.2) {
			self.view.window?.frame.origin.y += 200
		}
		self.driversInfo = driversInfo
	}
	
}

extension DriveInfoViewController: UnderlineTextfieldDelegate {
	func underlineTextfieldDidEndEditing(_ textField: UITextField) {
		guard textField.tag == 1337 else {
			return
		}
		self.localAddress = addressTextfield.text
	}
	
	func underlineTextfieldDidBeginEditing(_ textField: UITextField) {
		
	}
}
