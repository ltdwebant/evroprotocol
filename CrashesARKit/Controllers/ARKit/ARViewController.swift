import UIKit
import SceneKit
import ARKit
import JJHUD

class ARViewController: UIViewController, ARSCNViewDelegate, SCNSceneRendererDelegate {
	
	private var currentAngleY: Float = 0.0
	private var currentAngleX: Float = 0.0
	
	private var previousPan: CGPoint = CGPoint.zero

	var lastHitTest: [ARHitTestResult] = []
	
	// MARK: var properties of scene View
	fileprivate var boxNode: SCNNode!
	fileprivate var sceneView: ARSCNView!
	fileprivate var currentLine = [SCNNode]()
	fileprivate var screenCenter: CGPoint!
	fileprivate var dist1: CGFloat?
	fileprivate var dist2: CGFloat?
	
//	fileprivate var infoLabel = UILabel()
	
	fileprivate var currentMeaureState: ARkitMeasureState = .myCarMeasure {
		didSet {
			self.reset(schouldResetMeasure: false)
			self.nextButton.Enabled = false
			self.initLightWindow(with: self.currentMeaureState)
		}
	}
	fileprivate var measures: Measures!
	fileprivate var lightWindow: LightWindow! {
		didSet {
			self.lightWindow.windowWasClosed {
				self.nextButton.isHidden = false
				self.resetButton.isHidden = false
			}
		}
	}
	
	fileprivate var lightWindowController: LightWindowController!
	
	fileprivate var spheres: [SCNNode] = [] {
		didSet {
			switch spheres.count {
			case 2:
				let firstPoint = self.spheres.first!
				let secondPoint = self.spheres[1]
				let result = firstPoint.position.distance(from: secondPoint.position)
				self.dist1 = result
			//	self.display(distance: result)
				if currentMeaureState == .roadMeasure {
					self.considerMeasures()
					self.initializeBox()
					self.nextButton.Enabled = true
				}
			case 3:
				let firstPoint = self.spheres[1]
				let secondPoint = self.spheres.last!
				let result = firstPoint.position.distance(from: secondPoint.position)
				self.dist2 = result
			// self.display(distance: result)
				self.considerMeasures()
				self.initializeBox()
				self.nextButton.Enabled = true
			default:
				break
			}
		}
	}
	
	fileprivate var carTutorialView: CarTutorialView!
	
	fileprivate lazy var nextButton: NextButton = {
		let sceneWidth = sceneView.frame.width
		let sceneHeight = sceneView.frame.height
		let tempNextButton = NextButton(frame: CGRect(x: sceneWidth - sceneWidth / 4 - 30,
		                                              y: sceneHeight - sceneHeight / 8,
		                                              width: 100,
		                                              height: 45), titleState: TitleState.nextTitle)
		tempNextButton.handler(for: .touchUpInside, {
			guard tempNextButton.Enabled else {
				return
			}
			switch self.currentMeaureState {
			case .myCarMeasure:
				self.currentMeaureState = .otherCarMeasure
			case .otherCarMeasure:
				self.currentMeaureState = .roadMeasure
			case .roadMeasure:
				self.prepareDraw()
			}
		})
		return tempNextButton
	}()
	
	fileprivate lazy var resetButton: NextButton = {
		let sceneWidth = sceneView.frame.width
		let sceneHeight = sceneView.frame.height
		let tempResetButton = NextButton(frame: CGRect(x: sceneWidth - nextButton.frame.origin.x - nextButton.frame.width,
		                                              y: sceneHeight - sceneHeight / 8,
		                                              width: 100,
		                                              height: 45), titleState: TitleState.resetTitle)
		tempResetButton.handler(for: .touchUpInside, {
			self.reset()
		})
		return tempResetButton
	}()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.initEmptyMeasure()
		self.sceneView = ARSCNView(frame: self.view.frame)
		sceneView.debugOptions = []

		self.currentMeaureState = ARkitMeasureState.myCarMeasure
		
		self.screenCenter = self.sceneView.center
		self.view.addSubview(self.sceneView)
//		infoLabel = UILabel(frame: CGRect(x: 30, y: 60, width: 280, height: 100))
//		self.sceneView.addSubview(infoLabel)
		sceneView.delegate = self
		sceneView.showsStatistics = false
		
		let scene = SCNScene()
		
		addCrossSign()
		registerGestureRecognizers()
		
		sceneView.scene = scene
		self.view.addSubview(nextButton)
		self.view.addSubview(resetButton)
	}
	
	private func initLightWindow(with state: ARkitMeasureState) {
		self.nextButton.isHidden = true
		self.resetButton.isHidden = true
		carTutorialView = CarTutorialView(frame: sceneView.frame, currentState: state)
		lightWindow = LightWindow(view: carTutorialView, frame: sceneView.frame)
		lightWindow.present()
	}
	
	private func registerGestureRecognizers() {
		let pressButton = UITapGestureRecognizer(target: self, action: #selector(tapped))
		self.sceneView.addGestureRecognizer(pressButton)
	}
	
	@objc func tapped(recognizer :UILongPressGestureRecognizer) {
		let sceneView = recognizer.view as! ARSCNView
		if spheres.count >= 3 && self.currentMeaureState != .roadMeasure {
			return
		}
		if spheres.count >= 2 && self.currentMeaureState == .roadMeasure {
			return
		}
		let hitTestResults = sceneView.hitTest(self.screenCenter, types: .featurePoint)
		guard !hitTestResults.isEmpty else {
			return
		}
		guard let hitTestResult = hitTestResults.first else {
			return
		}
		self.lastHitTest = hitTestResults
		addPoint(hitTestResult: hitTestResult)
	}
	
	private func addPoint(hitTestResult: ARHitTestResult){
		let sphere = SCNSphere(radius: 0.015)
		let material = SCNMaterial()
		material.diffuse.contents = UIColor.red
		sphere.firstMaterial = material
		let sphereNode = SCNNode(geometry: sphere)
		sphereNode.position = SCNVector3(hitTestResult.worldTransform.columns.3.x, hitTestResult.worldTransform.columns.3.y, hitTestResult.worldTransform.columns.3.z)
		
		self.sceneView.scene.rootNode.addChildNode(sphereNode)
		self.spheres.append(sphereNode)
	}
	
	func renderer(_ renderer: SCNSceneRenderer, willRenderScene scene: SCNScene, atTime time: TimeInterval) {
		guard !self.spheres.isEmpty && self.spheres.count < 3 else {
			return
		}
		
		if self.currentMeaureState == .roadMeasure && self.spheres.count == 2 {
			return
		}
		
		let startPoint = self.spheres[self.spheres.count - 1].position
		let hitPoint = sceneView.hitTest(screenCenter, types: .featurePoint)
		
		if !hitPoint.isEmpty {
			DispatchQueue.main.async {
				if !self.currentLine.isEmpty {
					for node in self.currentLine {
						node.removeFromParentNode()
					}
				}
			 }
			glLineWidth(5.0)
			let lineEndPointPosition = self.sceneView.realWorldVector(screenPos: screenCenter)
			let line = lineFrom(vector: startPoint, toVector: lineEndPointPosition!)
			let lineNode = SCNNode(geometry: line)
			
			lineNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
			sceneView.scene.rootNode.addChildNode(lineNode)
			self.currentLine.append(lineNode)
		} else {
			print("hit test in renderer is empty")
		}
	}
	
	//function to display the distance
//	private func display(distance: CGFloat) {
//		infoLabel.text = "distance = \(distance)"
//	}
	
	private func reset(schouldResetMeasure: Bool = true) {
		let allNode = self.sceneView.scene.rootNode.childNodes
		for node in allNode{
			node.removeFromParentNode()
		}
		self.spheres = [SCNNode]()
		guard schouldResetMeasure else {
			return
		}
		guard measures != nil else {
			return
		}
		switch self.currentMeaureState {
		case .myCarMeasure:
			self.measures.myCarMeasure = CarMeasure()
		case .otherCarMeasure:
			self.measures.anotherCarMeasue = CarMeasure()
		case .roadMeasure:
			self.measures.roadWidth = 0.0
		}
	}
	
	private func addCrossSign() {
		let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 33))
		label.text = "+"
		label.textAlignment = .center
		label.center = self.sceneView.center
		
		self.sceneView.addSubview(label)
	}
	
	func lineFrom(vector vector1: SCNVector3, toVector vector2: SCNVector3) -> SCNGeometry {
		let indices: [Int32] = [0, 1]
		let source = SCNGeometrySource(vertices: [vector1, vector2])
		let element = SCNGeometryElement(indices: indices, primitiveType: .line)
		
		return SCNGeometry(sources: [source], elements: [element])
		
	}
	
	private func prepareDraw() {

		guard let driveInfoVC = self.storyboard?.instantiateViewController(withIdentifier:
			String(describing: DriveInfoViewController.self)) as? DriveInfoViewController else {
			return
		}
		self.nextButton.isHidden = true
		self.resetButton.isHidden = true
		self.lightWindowController = LightWindowController.init(controller: driveInfoVC, frame: sceneView.frame)
		self.lightWindowController.closeWindowHandler { driveInfo in
			let schemeVC = self.storyboard?.instantiateViewController(withIdentifier:
				String.init(describing: CrashThemeController.self)) as! CrashThemeController
			schemeVC.driverInfo = driveInfo
			schemeVC.measures = self.measures
			self.present(schemeVC, animated: true, completion: {
				self.sceneView.session.pause()
			})
		}
		self.lightWindowController.present()
	}
	
	@objc private func dragObjectOneFinger(sender: UIPanGestureRecognizer) {
		if sender.state == .changed {
			let tapPoint: CGPoint = sender.location(in: sceneView)
			let hitResults: [ARHitTestResult] = sceneView.hitTest(tapPoint, types: .featurePoint)
			let result: ARHitTestResult? = hitResults.last
			let matrix: SCNMatrix4? = SCNMatrix4(result!.worldTransform)
			let vector: SCNVector3? = SCNVector3Make(matrix!.m41, matrix!.m42, matrix!.m43)
			boxNode.position = vector!
		}
	}
	
	@objc private func dragObjectTwoFinger(sender: UIPanGestureRecognizer) {
		let translation = sender.translation(in: sender.view!)
		var newAngleX = (Float)(translation.y)*(Float)(Double.pi)/180.0
		newAngleX += currentAngleX
		var newAngleY = (Float)(translation.x)*(Float)(Double.pi)/180.0
		newAngleY += currentAngleY
		//self.boxNode.transform = SCNMatrix4MakeRotation(newAngleX, Float(translation.x), Float(translation.y), Float(0.0))
		self.boxNode.eulerAngles.x = newAngleX
		self.boxNode.eulerAngles.y = newAngleY
		
		if(sender.state == UIGestureRecognizerState.ended) {
			currentAngleX = newAngleX
			currentAngleY = newAngleY
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		let configuration = ARWorldTrackingSessionConfiguration()
		configuration.planeDetection = .horizontal
		
		sceneView.session.run(configuration)
	}
	
	func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
		switch camera.trackingState {
		case .limited(let reason):
			print("camera limited cause = \(reason)")
			JJHUD.showLoading(text: "Настраиваем камеру")
		case .normal:
			print("camera normal state")
			JJHUD.hide()
		case .notAvailable:
			JJHUD.showLoading(text: "Настраиваем камеру")
			print("camera not available")
		}
	}
	
	private func initEmptyMeasure() {
		self.measures = Measures()
	}
	
	private func considerMeasures() {
		switch self.currentMeaureState {
		case .myCarMeasure:
			guard self.dist1 != nil && self.dist2 != nil else {
				return
			}
			if Double(self.dist1!) > Double(self.dist2!) {
				self.measures.myCarMeasure?.length = self.dist1!
				self.measures.myCarMeasure?.width = self.dist2!
			} else {
				self.measures.myCarMeasure?.length = self.dist2!
				self.measures.myCarMeasure?.width = self.dist1!
			}
		case .otherCarMeasure:
			if Double(self.dist1!) > Double(self.dist2!) {
				self.measures.anotherCarMeasue?.length = self.dist1!
				self.measures.anotherCarMeasue?.width = self.dist2!
			} else {
				self.measures.anotherCarMeasue?.length = self.dist2!
				self.measures.anotherCarMeasue?.width = self.dist1!
			}
		case .roadMeasure:
			self.measures.roadWidth = self.dist1
		}
	}

	private func initializeBox() {
		var width: CGFloat!
		var height: CGFloat!
		switch self.currentMeaureState {
		case .myCarMeasure:
			width = self.measures.myCarMeasure!.width!
			height = self.measures.myCarMeasure!.length!
		case .otherCarMeasure:
			width = self.measures.anotherCarMeasue!.width!
			height = self.measures.anotherCarMeasue!.length!
		case .roadMeasure:
			width = self.measures.roadWidth
			height = 0.04
		}
		let box = SCNBox.init(width: width,
		                      height: 0.1,
		                      length: height,
		                      chamferRadius: 0)
		let matereal = SCNMaterial()
		matereal.diffuse.contents = UIColor.white
		box.firstMaterial = matereal
		
		let vectorLast = self.spheres.last!.worldTransform
		let vectorFirst = self.spheres.first!.worldTransform
		
		let tempVector = SCNVector3.init((vectorFirst.m41 + vectorLast.m41) / 2 ,
		                                 (vectorFirst.m42 + vectorLast.m42) / 2,
		                                 (vectorFirst.m43 + vectorLast.m43) / 2)
		print("temp vector = \(tempVector.x) \(tempVector.y), \(tempVector.z)")
		
		
		self.boxNode = SCNNode.init(geometry: box)
		self.boxNode.position = tempVector
		let pan1GestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.dragObjectOneFinger(sender:)))
		pan1GestureRecognizer.delegate = self
		pan1GestureRecognizer.minimumNumberOfTouches = 2
		self.sceneView.addGestureRecognizer(pan1GestureRecognizer)
		let pan2GestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.dragObjectTwoFinger(sender:)))
		pan2GestureRecognizer.delegate = self
		pan2GestureRecognizer.maximumNumberOfTouches = 1
		self.sceneView.addGestureRecognizer(pan2GestureRecognizer)
		
		self.sceneView.scene.rootNode.addChildNode(self.boxNode)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		sceneView.session.pause()
	}
	
	
}


extension ARViewController: UIGestureRecognizerDelegate {
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
}

