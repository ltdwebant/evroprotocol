//
//  CrashTheme.swift
//  CrashesARKit
//
//  Created by Danya on 08.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//
import MessageUI
import Foundation
import UIKit

final class CrashThemeController: UIViewController {
	
	open var driverInfo: DriversInformation!
	open var measures: Measures!
	
	fileprivate var yStart: CGFloat = 33
	fileprivate let labelHeight: CGFloat = 18
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.initializeLabels()
		self.drawScheme { yCoordinate in
			self.drawSignatureLabels(yCoordinate: yCoordinate)
			self.initializeMakePictureButton()
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		AppUtility.lockOrientation(.landscapeLeft, andRotateTo: .landscapeLeft)
	}
	
	private func drawScheme(complition: @escaping ((CGFloat) -> Void)) {
		let schemeView = CrashThemeView.init(frame: CGRect.init(x: 5,
		                                                        y: yStart + 5,
		                                                        width: self.view.frame.height / 3 * 2,
		                                                        height: self.view.frame.width * 2 / 5), measures: self.measures)
		self.view.addSubview(schemeView)
		let endFloat = yStart + 5 + schemeView.frame.size.height
		complition(endFloat)
	}
	
	private func initializeLabels() {
		
		let mainLabel = UILabel.init(frame: CGRect(x: 5,
		                                           y: 5,
		                                           width: self.view.frame.height - 10,
		                                           height: 5 + yStart - 3))
		mainLabel.numberOfLines = 1
		mainLabel.font = UIFont.boldSystemFont(ofSize: 19)
		mainLabel.text = "СХЕМА МЕСТА ДОРОЖНО-ТРАНСПОРТНОГО ПРОИСШЕСТВИЯ"
		mainLabel.textAlignment = .center
		self.view.addSubview(mainLabel)
		
		let splittedDate = self.split(date: driverInfo.crashDate!)
		let dateLabel = ThemeLabel.init(frame: CGRect.init(x: 5,
		                                                   y: yStart,
		                                                   width: self.view.frame.height - 10,
		                                                   height: labelHeight))
		dateLabel.text = "Дата и время происшествия - \(splittedDate.day) \(splittedDate.month) \(splittedDate.year) года в \(splittedDate.hours) час. \(splittedDate.minutes) мин., адрес: \(driverInfo.crashStreet!) "
		self.view.addSubview(dateLabel)
		
		yStart += labelHeight + 2
		
		let ownerCarLabel = ThemeLabel.init(frame: CGRect.init(x: 5, y: yStart, width: self.view.frame.height - 10, height: labelHeight))
		ownerCarLabel.text = "Транспортное средство: '1' Марка: \(driverInfo.ownerCarInfo!.mark!), Модель: \(driverInfo.ownerCarInfo!.model!), Регистрационный знак: \(driverInfo.ownerCarInfo!.registryNumber!)"
		self.view.addSubview(ownerCarLabel)
		
		yStart += labelHeight + 2

		let ownerNameLabel = ThemeLabel.init(frame: CGRect.init(x: 5, y: yStart, width: self.view.frame.height - 10, height: labelHeight))
		ownerNameLabel.text = "Данные о водителе '1' Фамилия: \(driverInfo.ownerInfo!.surname!), имя: \(driverInfo.ownerInfo!.name!), отчество: \(driverInfo.ownerInfo!.patronymic!)"
		self.view.addSubview(ownerNameLabel)
		
		yStart += labelHeight + 2

		let anotherCarLabel = ThemeLabel.init(frame: CGRect.init(x: 5, y: yStart, width: self.view.frame.height - 10, height: labelHeight))
		anotherCarLabel.text = "Транспортное средство: '2' Марка: \(driverInfo.anotherCarInfo!.mark!), Модель: \(driverInfo.anotherCarInfo!.model!), Регистрационный знак: \(driverInfo.anotherCarInfo!.registryNumber!)"
		self.view.addSubview(anotherCarLabel)
		
		yStart += labelHeight + 2

		let anotherNameLabel = ThemeLabel.init(frame: CGRect.init(x: 5, y: yStart, width: self.view.frame.height - 10, height: labelHeight))
		anotherNameLabel.text = "Данные о водителе '2' Фамилия: \(driverInfo.anotherDriverInfo!.surname!), имя: \(driverInfo.anotherDriverInfo!.name!), отчество: \(driverInfo.anotherDriverInfo!.patronymic!)"
		self.view.addSubview(anotherNameLabel)
		
		yStart += labelHeight + 2
	}
	
	private func drawSignatureLabels(yCoordinate: CGFloat) {
		let schemeLabel = ThemeLabel.init(frame: CGRect(x: 5,
		                                                y: yCoordinate + 5,
		                                                width: self.view.frame.height - 10,
		                                                height: labelHeight))
		schemeLabel.text = "Схему составили, со схемой согласны:"
		self.view.addSubview(schemeLabel)
		
		var yAx = yCoordinate
		yAx += labelHeight + 2
		
		let driverLabel = ThemeLabel.init(frame: CGRect(x: 5,
		                                                y: yAx,
		                                                width: self.view.frame.height - 10,
		                                                height: labelHeight))
		driverLabel.text = "Водители:    1                      (подпись)                       2                      (подпись)                    "
		self.view.addSubview(driverLabel)
		
		yAx += labelHeight + 2
		
		let tempoLabel = ThemeLabel.init(frame: CGRect(x: 5,
		                                                y: yAx,
		                                                width: self.view.frame.height - 10,
		                                                height: labelHeight))
		tempoLabel.text = "При составлении схемы производилось фотографирование / видеосъемка места ДТП и повреждений транспортных средств, обязуюсь представить фотографии / видеозапись по требованию сотрудников полиции:"
		self.view.addSubview(tempoLabel)
		
		yAx += labelHeight + 2
		
		let driverLabel2 = ThemeLabel.init(frame: CGRect(x: 5,
		                                               y: yAx,
		                                               width: self.view.frame.height - 10,
		                                               height: labelHeight))
		driverLabel2.text = "Водители:    1                      (подпись)                       2                      (подпись)                    "
		self.view.addSubview(driverLabel2)
	}
	
	private func initializeMakePictureButton() {
		let makePictureButton = NextButton.init(frame: CGRect.init(x: self.view.frame.height - 120,
		                                                         y: self.view.frame.width - 50,
		                                                         width: 110,
		                                                         height: 40), titleState: .makePicture)
		makePictureButton.handler(for: .touchUpInside) {
			makePictureButton.isHidden = true
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
				let viewImage = UIImage(view: self.view)
				self.sendOnEmail(image: viewImage)
			})
		}
		self.view.addSubview(makePictureButton)
	}
	
	private func sendOnEmail(image: UIImage) {
		if MFMailComposeViewController.canSendMail() {
			let mail = MFMailComposeViewController()
			mail.mailComposeDelegate = self
			mail.setSubject("Ваш Европротокол.")
			mail.setMessageBody("Сохраните и распечатайте картинку", isHTML: false)
			let imageDate = UIImagePNGRepresentation(image)
			mail.addAttachmentData(imageDate!, mimeType: "image/png", fileName: "Evroprotocol.Scheme")
			self.present(mail, animated: true, completion: nil)
		}
	}
	
	private func split(date realDate: Date) -> (year: String, month: String, day: String, hours: String, minutes: String) {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyy"
		let year = formatter.string(from: realDate)
		formatter.dateFormat = "d"
		let day = formatter.string(from: realDate)
		formatter.dateFormat = "MMMM"
		let mounth = formatter.string(from: realDate)
		formatter.dateFormat = "HH"
		let hours = formatter.string(from: realDate)
		formatter.dateFormat = "mm"
		let minutes = formatter.string(from: realDate)
		return (year, mounth, day, hours, minutes)
	}
}


extension CrashThemeController: MFMailComposeViewControllerDelegate {
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		controller.dismiss(animated: true, completion: nil)
	}
}
