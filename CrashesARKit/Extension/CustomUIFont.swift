//
//  CustomUIFont.swift
//  CrashesARKit
//
//  Created by Danya on 14.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

extension UIFont {
	
	static func regularHelvetica(withSize size: CGFloat) -> UIFont {
		guard let font = UIFont(name: "HelveticaNeue-Light", size: size) else {
			fatalError("Cant get HelveticaNeue-Light UIFont")
		}
		return font
	}
}
