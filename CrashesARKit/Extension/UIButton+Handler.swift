//
//  UIButton+Handler.swift
//  CrashesARKit
//
//  Created by Danya on 04.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

class ClosureSleeve {
	let closure: () -> Void
	init (_ closure: @escaping () -> Void) {
		self.closure = closure
	}
	
	@objc func invoke() {
		closure()
	}
}

extension UIControl {
	
	func randomNumber(min: Int, max: Int) -> Int {
		return Int(arc4random_uniform(UInt32(max)) + UInt32(min))
	}
	
	func handler (for controlEvents: UIControlEvents, _ closure: @escaping () -> Void) {
		let sleeve = ClosureSleeve(closure)
		addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
		let currentMiliseconds: Int = Int(Date().timeIntervalSince1970)
		let random = randomNumber(min: 4, max: 158394) + currentMiliseconds
		objc_setAssociatedObject(self, String(format: "[%d]", random), sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
	}
}
