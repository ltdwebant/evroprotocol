//
//  SCNGeometry+SqareForm.swift
//  CrashesARKit
//
//  Created by Danya on 07.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//
import SceneKit
import Foundation

extension SCNGeometry {
	class func sqareFrom(vector1: SCNVector3, vector2: SCNVector3, vector3: SCNVector3, vector4: SCNVector3) -> SCNGeometry {
		let indices: [Int32] = [0, 1, 2, 3]
		let source = SCNGeometrySource(vertices: [vector1, vector2, vector3, vector4])
		let element = SCNGeometryElement(indices: indices, primitiveType: .triangles)
		return SCNGeometry(sources: [source], elements: [element])
	}
}
