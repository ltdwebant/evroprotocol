//
//  Enum.swift
//  CrashesARKit
//
//  Created by Danya on 04.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import Foundation

enum ARkitMeasureState {
	case myCarMeasure
	case otherCarMeasure
	case roadMeasure
}

enum PageInfoState {
	case myInfo
	case anotherDriverInfo
}
