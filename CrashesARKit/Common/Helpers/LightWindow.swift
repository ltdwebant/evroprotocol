//
//  LightWindow.swift
//  CrashesARKit
//
//  Created by Danya on 01.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

typealias WindowClosedHandler = (() -> Void)

class LightWindow: UIViewController {
	
	private var closeByTouch = true
	
	fileprivate var ownController: UIViewController
	fileprivate var ownWindow: UIWindow?
	fileprivate var localHandler: WindowClosedHandler!
	
	init(view: UIView, frame: CGRect) {
		self.ownController = UIViewController()
		self.ownController.view.isHidden = false
		self.ownController.modalTransitionStyle = .crossDissolve
		self.ownController.modalPresentationStyle = .formSheet
		self.ownController.view.frame = frame
		super.init(nibName: nil, bundle: nil)
		
		self.ownController.view.addSubview(view)
		let window = UIWindow(frame: frame)
		window.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
		window.isHidden = false
		
		window.rootViewController = self
		window.isHidden = true
		window.windowLevel = UIWindowLevelAlert + 1
		window.makeKeyAndVisible()
		self.ownWindow = window
		
		if closeByTouch {
			let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissWindow))
			self.ownController.view.addGestureRecognizer(tapGesture)
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	@objc func dismissWindow() {
		ownWindow?.isHidden = true
		ownWindow = nil
		ownController.view = UIView()
		self.dismiss(animated: false)
		self.localHandler()
	}
	
	func windowWasClosed(complition: @escaping WindowClosedHandler) {
		self.localHandler = complition
	}
	
	func present() {
		self.present { [weak self] in
			self?.ownWindow?.isHidden = false
			self?.view.isHidden = false
		}
	}
	
	private func present(with completion: (() -> Void)?) {
		self.present(self.ownController, animated: false, completion: completion)
	}

}
