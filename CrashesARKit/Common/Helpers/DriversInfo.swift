//
//  driversInfo.swift
//  CrashesARKit
//
//  Created by Danya on 11.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

struct CarInfo {
	var registryNumber: String?
	var mark: String?
	var model: String?
}

struct DriverInfo {
	var name: String?
	var surname: String?
	var patronymic: String?
}

class DriversInformation {
	var ownerInfo: DriverInfo?
	var anotherDriverInfo: DriverInfo?
	
	var ownerCarInfo: CarInfo?
	var anotherCarInfo: CarInfo?
	
	var crashDate: Date?
	var crashStreet: String?
	
	init() {
		self.ownerInfo = DriverInfo()
		self.anotherDriverInfo = DriverInfo()
		
		self.ownerCarInfo = CarInfo()
		self.anotherCarInfo = CarInfo()
	}
	
}
