//
//  AppUtility.swift
//  CrashesARKit
//
//  Created by Danya on 13.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

struct AppUtility {
	
	static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
		if let delegate = UIApplication.shared.delegate as? AppDelegate {
			delegate.orientationLock = orientation
		}
	}
	
	static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
		self.lockOrientation(orientation)
		UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
	}
}
