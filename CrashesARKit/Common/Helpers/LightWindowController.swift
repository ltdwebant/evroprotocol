//
//  WindowWithControllerViewController.swift
//  CardioLite
//
//  Created by Danya on 13.04.17.
//  Copyright © 2017 Владислав Прусаков. All rights reserved.
//

import UIKit

typealias WindowHandlerWithInfo = ((DriversInformation) -> Void)

class LightWindowController: UIViewController {
	
	fileprivate var ownController: UIViewController
	fileprivate var ownWindow: UIWindow?
	
	fileprivate var localHandler: WindowHandlerWithInfo!
	
	init(controller viewController: UIViewController, frame: CGRect) {
		self.ownController = viewController
		self.ownController.preferredContentSize = CGSize.init(width: frame.width, height: frame.height)
		self.ownController.view.frame = frame
		self.ownController.view.alpha = 0.762
		self.ownController.modalPresentationStyle = .formSheet
		self.ownController.view.isHidden = false
		
		super.init(nibName: nil,
		           bundle: nil)
		let window = UIWindow(frame: frame)
		
		window.rootViewController = self
		window.isHidden = true
		window.windowLevel = UIWindowLevelAlert + 1
		window.backgroundColor = UIColor.lightGray.withAlphaComponent(0.0)
		window.makeKeyAndVisible()
		self.ownWindow = window
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func dismiss() {
		ownWindow?.isHidden = true
		ownWindow = nil
		self.dismiss(animated: false, completion: nil )
	}
	
	
	
	func present() {
		self.present { [weak self] in
			self?.ownWindow?.isHidden = false
			self?.view.isHidden = false
		}
		
	}
	
	private func present(with completion: (() -> Void)?) {
		if self.ownController is DriveInfoViewController {
			(self.ownController as! DriveInfoViewController).closeDelegate = self
		}
		self.present(self.ownController, animated: false, completion: completion)
	}
	
 func closeWindowHandler(complition: @escaping WindowHandlerWithInfo) {
		self.localHandler = complition
	}
	
}

extension LightWindowController: DriveInfoWindowCloseDelegate {
	func schouldCloseWindow(with driversInfo: DriversInformation) {
		self.dismiss()
		localHandler(driversInfo)
	}
	
}

