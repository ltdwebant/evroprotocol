//
//  CheckingClasses.swift
//  CrashesARKit
//
//  Created by Danya on 14.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import Foundation

class CheckingClasses {
	static func checkEmpty(driverInformation driver: DriversInformation) -> Bool {

		guard driver.crashDate != nil, driver.crashStreet != nil else {
			return false
		}
		
		guard driver.ownerInfo != nil,
			driver.anotherDriverInfo != nil,
			driver.ownerCarInfo != nil,
			driver.ownerInfo != nil else {
			return false
		}
		
		guard driver.ownerCarInfo?.mark != "",
			driver.ownerCarInfo?.model != "",
			driver.ownerCarInfo?.registryNumber != "" else {
			return false
		}
		
		guard driver.ownerInfo?.name != nil,
			driver.ownerInfo?.surname != nil,
			driver.ownerInfo?.patronymic != nil else {
			return false
		}
		
		guard driver.anotherCarInfo?.mark != nil,
			driver.anotherCarInfo?.model != nil,
			driver.anotherCarInfo?.registryNumber != nil else {
			return false
		}
		
		guard driver.anotherDriverInfo?.name != nil,
			driver.anotherDriverInfo?.patronymic != nil,
			driver.anotherDriverInfo?.surname != nil else {
			return false
		}
		
		return true
	}
}
