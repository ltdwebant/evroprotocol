//
//  Measure.swift
//  CrashesARKit
//
//  Created by Danya on 04.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

class CarMeasure {
	var width: CGFloat?
	var length: CGFloat?
}

class Measures {
	var myCarMeasure: CarMeasure?
	var anotherCarMeasue: CarMeasure?
	var roadWidth: CGFloat?
	
	init() {
		self.myCarMeasure = CarMeasure()
		self.anotherCarMeasue = CarMeasure()
	}
}
