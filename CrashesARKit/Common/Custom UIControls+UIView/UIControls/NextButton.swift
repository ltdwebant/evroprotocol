//
//  NextButton.swift
//  CrashesARKit
//
//  Created by Danya on 04.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

enum TitleState {
	case resetTitle
	case nextTitle
	case makePicture
	case back
}

@IBDesignable
final class NextButton: UIButton {
	
	fileprivate var currentButtonTitleState: TitleState = .nextTitle
	
	@IBInspectable
	var backTag: Int = -1 {
		didSet {
			if backTag == 1337 {
				self.setTitle("Назад", for: .normal)
			}
		}
	}
	
	private(set) var _enabled: Bool = false
	
	open var Enabled: Bool {
		get {
			return self._enabled
		}
		set {
			switch newValue {
			case true:
				self.backgroundColor = .white
			case false:
				self.backgroundColor = .gray
			}
			
			_enabled = newValue
		}
	}
	
	init(frame: CGRect, titleState: TitleState) {
		super.init(frame: frame)
		self.backgroundColor = .white
		self.currentButtonTitleState = titleState
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	
	override func layoutSubviews() {
		super.layoutSubviews()
		self.customizeButton()
	}
	
	private func customizeButton() {
		self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
		self.layer.cornerRadius = self.frame.height / 2
		self.layer.borderWidth = 0.4
		self.contentEdgeInsets = .init(top: 2, left: 4, bottom: 2, right: 4)
		switch currentButtonTitleState {
		case .resetTitle:
			self.setTitle("Cбросить", for: .normal)
		case .nextTitle:
			guard backTag != 1337 else {
				break
			}
			self.setTitle("Далее", for: .normal)
		case .makePicture:
			self.setTitle("Отправить", for: .normal)
		case .back:
			self.setTitle("Назад", for: .normal)
		}
		self.layer.borderColor = UIColor.lightGray.cgColor
		self.titleLabel?.textColor = UIColor.lightGray
		self.tintColor = UIColor.lightGray
		
		self.initializeShadow()
	}
	
	private func initializeShadow() {
		self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
		self.layer.shadowOffset = CGSize(width: 1.3, height: 2.0)
		self.layer.shadowRadius = 3
		self.layer.shadowOpacity = 1.0
		self.layer.masksToBounds = false
	}
}
