//
//  ThemeLabel.swift
//  CrashesARKit
//
//  Created by Danya on 13.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

class ThemeLabel: UILabel {
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.acceptSettings()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func acceptSettings() {
		self.numberOfLines = 2
		self.font = UIFont.regularHelvetica(withSize: 11.5)
		self.adjustsFontSizeToFitWidth = true
	}
}
