//
//  UnderlineTextfield.swift
//  CrashesARKit
//
//  Created by Danya on 11.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

protocol UnderlineTextfieldDelegate: class {
	func underlineTextfieldDidBeginEditing(_ textField: UITextField)
	func underlineTextfieldDidEndEditing(_ textField: UITextField)
}

@IBDesignable
final class UnderlineTextfield: UITextField {
	
	weak var underlineDelegate: UnderlineTextfieldDelegate!
	var fieldState: TextFieldState = TextFieldState.default
	
	override func awakeFromNib() {
		super.awakeFromNib()
		self.delegate = self
	}
	
	override func draw(_ rect: CGRect) {
		super.draw(rect)
		guard let context = UIGraphicsGetCurrentContext() else {
			assertionFailure("Can't get UIGraphicsGetCurrentContext")
			return
		}
		self.drawUnderline(context: context, rect: rect)
	}
	
	private func drawUnderline(context: CGContext, rect: CGRect) {
		context.saveGState()
		context.beginPath()
		
		let bottomLeftPoint = CGPoint(x: 0, y: rect.height)
		let bottomRightPoint = CGPoint(x: rect.width, y: rect.height)
		
		let strokeColor = self.fieldState.color
		context.setStrokeColor(strokeColor.cgColor)
		context.setLineWidth(3)
		
		context.move(to: bottomLeftPoint)
		context.addLine(to: bottomRightPoint)
		
		context.strokePath()
		context.restoreGState()
	}
	
	func changeFieldColor(for state: TextFieldState) {
		self.fieldState = state
		self.setNeedsDisplay()
	}

}

enum TextFieldState {
	case `default`
	case empty
	var color: UIColor {
		switch self {
		case .empty:
			return try! UIColor(hex: "#FF0000")
		case .default:
			return UIColor.black.withAlphaComponent(0.7)
		}
	}
	
}

extension UnderlineTextfield: UITextFieldDelegate {
	func textFieldDidBeginEditing(_ textField: UITextField) {
		guard self.underlineDelegate != nil else {
			return
		}
		self.underlineDelegate.underlineTextfieldDidBeginEditing(textField)
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		guard self.underlineDelegate != nil else {
			return
		}
		self.underlineDelegate.underlineTextfieldDidEndEditing(textField)
	}
}
