//
//  CrashThemeView.swift
//  CrashesARKit
//
//  Created by Danya on 13.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

final class CrashThemeView: UIView {
	
	private var measures: Measures!
	
	init(frame: CGRect, measures: Measures) {
		super.init(frame: frame)
		self.measures = measures
		self.backgroundColor = .white
		self.layer.borderWidth = 1
		self.layer.borderColor = UIColor.black.cgColor
		self.setNeedsDisplay()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func draw(_ rect: CGRect) {
		super.draw(rect)
		guard measures != nil else {
			return
		}
		guard let context = UIGraphicsGetCurrentContext() else {
			return
		}
		
		self.drawSchema(context: context, rect: rect)
	}
	
	private func drawSchema(context: CGContext, rect: CGRect) {
		context.saveGState()
		context.beginPath()
		
		let strokeColor = UIColor.black.withAlphaComponent(0.9)
		context.setStrokeColor(strokeColor.cgColor)
		context.setLineWidth(2)
		
		let height = rect.height
		let width = rect.width
		
		let startFirstCar: CGFloat = width / 3
		
		context.move(to: CGPoint.init(x: 0, y: height / 5))
		context.addLine(to: CGPoint.init(x: width, y: height / 5))
		
		context.move(to: CGPoint.init(x: 0, y: height - height / 5))
		context.addLine(to: CGPoint.init(x: width, y: height - height / 5))
		
		//Берем в расчет, что ширина - это 10 метров
		let roadWidth: CGFloat = 10
		
		let oneCarWidth = self.measures.myCarMeasure!.width! * height / roadWidth
		let oneCarLength = self.measures.myCarMeasure!.length! * width / roadWidth
		let anotherCarWidth = self.measures.anotherCarMeasue!.width! * height / roadWidth
		let anotherCarLength  = self.measures.anotherCarMeasue!.length! * width / roadWidth
		
		// 1 машина
		
		context.move(to: CGPoint.init(x: startFirstCar, y: height / 3))
		context.addLine(to: CGPoint.init(x: startFirstCar + oneCarLength,
		                                 y: height / 3))
		context.addLine(to: CGPoint.init(x: startFirstCar + oneCarLength,
		                                 y: height / 3 + oneCarWidth))
		context.addLine(to: CGPoint.init(x: startFirstCar, y: height / 3 + oneCarWidth))
		context.addLine(to: CGPoint.init(x: startFirstCar, y: height / 3))
		
		// 2 машина
		
		let distanceBetweenTwoCars: CGFloat = 4
		let startAnotherCar: CGFloat = startFirstCar + oneCarLength + distanceBetweenTwoCars
		
		context.move(to: CGPoint.init(x: startAnotherCar, y: height / 3))
		context.addLine(to: CGPoint.init(x: startAnotherCar + anotherCarLength,
		                                 y: height / 3))
		context.addLine(to: CGPoint.init(x: startAnotherCar + anotherCarLength,
		                                 y: height / 3 + anotherCarWidth))
		context.addLine(to: CGPoint.init(x: startAnotherCar,
		                                 y: height / 3 + anotherCarWidth))
		context.addLine(to: CGPoint.init(x: startAnotherCar, y: height / 3))
		
		context.strokePath()
		context.restoreGState()
	}
}

