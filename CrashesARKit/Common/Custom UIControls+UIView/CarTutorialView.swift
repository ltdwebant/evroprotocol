//
//  CarTutorialView.swift
//  CrashesARKit
//
//  Created by Danya on 04.09.17.
//  Copyright © 2017 Danya. All rights reserved.
//

import UIKit

final class CarTutorialView: UIView {
	
	private var animatedIphoneImage1: UIImageView!
	private var animatedIphoneImage2: UIImageView!
	
	fileprivate var currentMeasureState: ARkitMeasureState!

	init(frame: CGRect, currentState: ARkitMeasureState) {
		super.init(frame: frame)
		self.currentMeasureState = currentState
		self.initialize()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func initialize() {
		self.backgroundColor = .clear
		let infoLabel = UILabel(frame: CGRect(x: 20,
		                                      y: 25,
		                                      width: self.frame.width - 40,
		                                      height: self.frame.height / 10 - 20))
		infoLabel.textAlignment = .center
		infoLabel.font = UIFont.boldSystemFont(ofSize: 19)
		
		switch currentMeasureState {
		case .myCarMeasure, .otherCarMeasure:
			if currentMeasureState == .myCarMeasure {
				infoLabel.text = "Измерьте свой автомобиль"
			} else {
				infoLabel.text = "Измерьте чужой автомобиль"
			}
			let sideCarImage = UIImageView(frame: CGRect(x: 30,
			                                             y: self.frame.height / 10,
			                                             width: self.frame.width - 60,
			                                             height: self.frame.height / 10 * 4))
			sideCarImage.contentMode = .scaleAspectFit
			sideCarImage.image = #imageLiteral(resourceName: "jeepSide")
			animatedIphoneImage1 = UIImageView(frame: CGRect(x: 25,
			                                                 y: self.frame.height / 9 + 30,
			                                                 width: self.frame.width / 4,
			                                                 height: self.frame.height / 5))
			animatedIphoneImage1.contentMode = .scaleAspectFit
			animatedIphoneImage1.image = #imageLiteral(resourceName: "iphoneIcon")
			sideCarImage.addSubview(animatedIphoneImage1)
			
			let backCarImage = UIImageView(frame: CGRect(x: 30,
			                                             y: self.frame.height / 2,
			                                             width: self.frame.width - 60,
			                                             height: self.frame.height / 10 * 4))
			backCarImage.contentMode = .scaleAspectFit
			backCarImage.image = #imageLiteral(resourceName: "jeepBack")
			animatedIphoneImage2 = UIImageView(frame: CGRect(x: 5,
			                                                 y: self.frame.height / 5 + 30,
			                                                 width: self.frame.width / 4,
			                                                 height: self.frame.height / 5))
			animatedIphoneImage2.contentMode = .scaleAspectFit
			animatedIphoneImage2.image = #imageLiteral(resourceName: "iphoneIcon")
			backCarImage.addSubview(animatedIphoneImage2)
			self.addSubview(sideCarImage)
			self.addSubview(backCarImage)
		case .roadMeasure:
			let roadImage = UIImageView(frame: CGRect(x: 30,
			                                          y: self.frame.height / 6,
			                                          width: self.frame.width - 60,
			                                          height: self.frame.height / 2 + 40))
			roadImage.contentMode = .scaleAspectFit
			roadImage.image = #imageLiteral(resourceName: "road")
			infoLabel.text = "Измерьте ширину дороги от начала до конца"
			infoLabel.numberOfLines = 2
			animatedIphoneImage1 = UIImageView(frame: CGRect(x: 0,
			                                                 y: roadImage.frame.height * 3 / 4,
			                                                 width: self.frame.width / 4 + 10,
			                                                 height: self.frame.height / 5))
			animatedIphoneImage1.contentMode = .scaleAspectFit
			animatedIphoneImage1.image = #imageLiteral(resourceName: "iphoneIcon")
			roadImage.addSubview(animatedIphoneImage1)
			self.addSubview(roadImage)
		default:
			break
		}
		self.addSubview(infoLabel)
		DispatchQueue.main.async {
			Timer.scheduledTimer(timeInterval: 1.5,
			                     target: self,
			                     selector: #selector(self.animateIphone),
			                     userInfo: nil,
			                     repeats: false)
		}
	}
	
	@objc private func animateIphone() {
		UIView.animate(withDuration: 1.3,
		               delay: 0.2,
		               options: [.repeat],
		               animations: {
										if self.animatedIphoneImage1 != nil {
											self.animatedIphoneImage1.center.x += 200
										}
										if self.animatedIphoneImage2 != nil {
											self.animatedIphoneImage2.center.x += 215
										}
			self.setNeedsDisplay()
		}, completion: nil)
	}
	
	
}
